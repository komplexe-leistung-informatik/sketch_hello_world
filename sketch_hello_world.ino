// Befehlsbibliothek für das LCD
#include <LiquidCrystal.h>                        

// Initialisieren der Ports des LCD 
//    D5 (Datenbus)
//    D4 (Datenbus)
//    R/W (Read or Write)
//    RS (Bestimmung ob Datenbit als Befehl oder Zeichendaten interpretiert werden)
//    Vee (Kontrastregelung)
//    Vdd (Stromversorgung))
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);            

void setup()                                      
{
  // Groesse des Displays initialisieren
  lcd.begin(16, 2);                               
}

void loop()                                       
{
  // Cursor steuern (2. Stelle, 1. Zeile) und Text ausgeben 
  lcd.setCursor(3, 0) ;                           
  lcd.print("Projekt 1:");                        

  // Cursor steuern (1. Stelle, 2. Zeile) und Text ausgeben 
  lcd.setCursor(2, 1);
  lcd.print("Hello World!");
}
